## Desired behaviour
<!-- Detail the functionality of the feature itself. -->
<!-- If you wish to change existing behaviour (enhancement), describe the difference. -->

## Context
<!-- Add any related context (similar features in other apps, related issues, ...) -->

## Design
<!-- Leave this empty when creating the issue. -->
<!-- Assignee SHOULD fill in the design proposal. -->

## Impact
<!-- Leave this empty when creating the issue. -->
<!-- Assignee MUST describe all parts of the application that could be affected by the change. -->
